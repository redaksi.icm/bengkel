-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Okt 2020 pada 11.27
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bengkel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(40) NOT NULL,
  `harga_beli` int(50) NOT NULL,
  `harga_jual` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`kode_barang`, `nama_barang`, `harga_beli`, `harga_jual`) VALUES
('BR-0001', 'OLI FEDERAL MATIC 1L', 34000, 40000),
('BR-0002', 'SOKET OLI MIO', 6000, 9000),
('BR-0003', 'FILTER UDARA MIO', 45000, 52000),
('BR-0004', 'TUTUP KLEEP VEGA', 15000, 23000),
('BR-0005', 'TOPSET MX', 18000, 22000),
('BR-0006', 'LAHAR 6003', 32000, 38000),
('BR-0007', 'PISTON KIT XEON STD', 232000, 245000),
('BR-0008', 'BLOK XEON', 232000, 240000),
('BR-0009', 'OLI PRIMA XP 1,0L', 35000, 40000),
('BR-0010', 'AKI GTZ55 KARISMA', 186000, 192000),
('BR-0011', 'BUSI', 15000, 20000),
('BR-0012', 'OLI FEDERAL MATIC 0,8L', 34, 40),
('BR-0018', 'BUSI FDR MIO', 13000, 15000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_service`
--

CREATE TABLE `tb_detail_service` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_service` varchar(50) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `nama_mekanik` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `total_harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_detail_service`
--

INSERT INTO `tb_detail_service` (`id`, `tgl`, `no_service`, `nama_pelanggan`, `nama_mekanik`, `nama_barang`, `jumlah`, `total_harga`) VALUES
(68, '2020-01-16', 'SRV00001', 'apis', 'YENDI U', 'OLI FEDERAL MATIC 1L', 1, 40000),
(69, '2020-01-17', 'SRV00002', 'adr', 'YENDI U', 'AKI GTZ55 KARISMA', 2, 384000),
(70, '2020-01-19', 'SRV00003', 'Sahril', 'YENDI U', 'SOKET OLI MIO', 1, 9000),
(71, '2020-01-19', 'SRV00004', 'bbb', 'Rikas', 'OLI FEDERAL MATIC 1L', 1, 40000),
(72, '2020-01-19', 'SRV00004', 'bbb', 'Rikas', 'SOKET OLI MIO', 1, 9000),
(73, '2020-01-24', 'SRV00006', 'FAKHRU', 'Yendi', 'OLI FEDERAL MATIC 1L', 1, 40000),
(74, '2020-01-24', 'SRV00006', 'FAKHRU', 'Yendi', 'BUSI', 1, 20000),
(75, '2020-01-24', 'SRV00006', 'FAKHRU', 'Yendi', 'FILTER UDARA MIO', 1, 52000),
(76, '2020-01-24', 'SRV00007', 'Hendri', 'Rikas', 'OLI FEDERAL MATIC 1L', 1, 40000),
(77, '2020-09-11', 'SRV00008', 'riri', 'Yendi', 'OLI FEDERAL MATIC 1L', 1, 40000),
(78, '2020-09-11', 'SRV00008', 'riri', 'Yendi', 'FILTER UDARA MIO', 1, 52000),
(79, '2020-09-14', 'SRV00010', 'uuuu', 'Yendi', 'OLI FEDERAL MATIC 1L', 1, 40000),
(80, '2020-09-14', 'SRV00010', 'uuuu', 'Yendi', 'SOKET OLI MIO', 1, 9000),
(81, '2020-09-14', 'SRV00011', 'kitiangba ', 'Rikas', 'SOKET OLI MIO', 1, 9000),
(82, '2020-09-20', 'SRV00012', 'ilham', 'Yendi', 'OLI FEDERAL MATIC 1L', 1, 40000),
(83, '2020-09-24', 'SRV00013', 'andri', 'Rikas', 'OLI FEDERAL MATIC 1L', 1, 40000),
(84, '2020-09-24', 'SRV00014', 'aan', 'Yendi', 'SOKET OLI MIO', 1, 9000),
(85, '2020-09-24', 'SRV00013', 'andri', 'Rikas', 'SOKET OLI MIO', 1, 9000),
(86, '2020-09-25', 'SRV00015', 'bbbb', 'Rikas', 'OLI FEDERAL MATIC 1L', 1, 40000),
(87, '2020-09-25', 'SRV00015', 'bbbb', 'Rikas', 'TOPSET MX', 1, 22000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_faktur`
--

CREATE TABLE `tb_faktur` (
  `id` int(11) NOT NULL,
  `no_faktur` int(50) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_faktur`
--

INSERT INTO `tb_faktur` (`id`, `no_faktur`, `tanggal`, `status`) VALUES
(3, 200100002, '2020-01-13', 'y'),
(4, 200100003, '2020-01-14', 'y'),
(5, 200100004, '2020-01-19', 'y'),
(6, 200100005, '2020-01-19', 'y'),
(7, 200100006, '2020-01-24', 'y'),
(8, 200900007, '2020-09-15', 'y'),
(9, 200900008, '2020-09-25', 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_faktur_service`
--

CREATE TABLE `tb_faktur_service` (
  `id` int(11) NOT NULL,
  `no_service` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis_service` varchar(50) NOT NULL,
  `id_pelanggan` varchar(35) NOT NULL,
  `nama_mekanik` varchar(35) NOT NULL,
  `status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_faktur_service`
--

INSERT INTO `tb_faktur_service` (`id`, `no_service`, `tanggal`, `jenis_service`, `id_pelanggan`, `nama_mekanik`, `status`) VALUES
(20, 'SRV00001', '2020-01-16', 'full service', 'apis', 'YENDI U', 'y'),
(22, 'SRV00002', '2020-01-17', 'service ringan', 'adr', 'YENDI U', 'y'),
(23, 'SRV00003', '2020-01-19', 'service ringan', 'Sahril', 'YENDI U', 'y'),
(24, 'SRV00004', '2020-01-19', 'full service', 'bbb', 'Rikas', 'y'),
(25, 'SRV00005', '2020-01-19', 'full service', 'Adek', 'Rikas', 'x'),
(26, 'SRV00006', '2020-01-24', 'service ringan', 'FAKHRU', 'Yendi', 'y'),
(27, 'SRV00007', '2020-01-24', 'service ringan', 'Hendri', 'Rikas', 'y'),
(28, 'SRV00008', '2020-09-11', 'service ringan', 'riri', 'Yendi', 'x'),
(29, 'SRV00009', '2020-09-11', 'service ringan', 'riri', 'Rikas', 'k'),
(30, 'SRV00010', '2020-09-14', 'service ringan', 'uuuu', 'Yendi', 'y'),
(31, 'SRV00011', '2020-09-14', 'service ringan', 'kitiangba ', 'Rikas', 'y'),
(32, 'SRV00012', '2020-09-20', 'service ringan', 'ilham', 'Yendi', 'y'),
(33, 'SRV00013', '2020-09-24', 'service ringan', 'andri', 'Rikas', 'y'),
(34, 'SRV00014', '2020-09-24', 'full service', 'aan', 'Yendi', 'y'),
(35, 'SRV00015', '2020-09-25', 'service ringan', 'bbbb', 'Rikas', 'y'),
(36, 'SRV00016', '2020-09-26', 'full service', 'Hendri', 'Yendi', 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_service`
--

CREATE TABLE `tb_jenis_service` (
  `id` int(11) NOT NULL,
  `jenis_service` varchar(50) NOT NULL,
  `biaya` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_service`
--

INSERT INTO `tb_jenis_service` (`id`, `jenis_service`, `biaya`) VALUES
(9, 'full service', 50000),
(12, 'service ringan', 35000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mekanik`
--

CREATE TABLE `tb_mekanik` (
  `id_mekanik` varchar(50) NOT NULL,
  `nama_mekanik` varchar(35) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_mekanik`
--

INSERT INTO `tb_mekanik` (`id_mekanik`, `nama_mekanik`, `no_hp`) VALUES
('MK-0003', 'Yendi', '0823568780'),
('MK-0004', 'Rikas', '082345475485');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `no_kendaraan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`id`, `nama`, `no_kendaraan`) VALUES
('PL-0004', 'Adek', 'BA 2213 AC'),
('PL-0005', 'Sahril', 'BA 0001 CD'),
('PL-0007', 'Hendri', 'BA 1324 YN'),
('PL-0008', 'FAKHRU', 'BA 2249 BE'),
('PL-0009', 'riri', 'B 4439 BA'),
('PL-0010', 'rendi', 'BA 4478 BA'),
('PL-0011', 'uuuu', 'BA 5555 BA'),
('PL-0012', 'kitiangba ', 'BA 2222 BA'),
('PL-0013', 'ilham', 'BA 8888 BA'),
('PL-0014', 'andri', 'BA 3333 BA'),
('PL-0015', 'aan', 'B 3232 AF'),
('PL-0016', 'bbbb', 'BA 111 B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_faktur` varchar(50) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga` int(50) NOT NULL,
  `jumlah` int(50) NOT NULL,
  `total_harga` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`id`, `tgl`, `no_faktur`, `kode_barang`, `nama_barang`, `harga`, `jumlah`, `total_harga`) VALUES
(53, '2020-01-23', '200100003', 'BR-0002', 'SOKET OLI MIO', 9000, 1, 9000),
(54, '2020-01-23', '200100003', 'BR-0017', 'OLI FEDERAL BEBEK 0,8 L', 40000, 10, 400000),
(55, '2020-01-23', '200100003', 'BR-0001', 'OLI FEDERAL MATIC 0,8L', 40000, 12, 480000),
(56, '2020-01-23', '200100003', 'BR-0001', 'OLI FEDERAL MATIC 0,8L', 40000, 4, 160000),
(57, '2020-01-23', '200100003', 'BR-0001', 'OLI FEDERAL MATIC 0,8L', 40000, 1, 40000),
(58, '2020-01-23', '200100002', 'BR-0001', 'OLI FEDERAL MATIC 1L', 40000, 1, 40000),
(59, '2020-01-19', '200100004', 'BR-0001', 'OLI FEDERAL MATIC 1L', 40000, 1, 40000),
(60, '2020-01-19', '200100004', 'BR-0002', 'SOKET OLI MIO', 9000, 2, 18000),
(61, '2020-01-19', '200100005', 'BR-0003', 'FILTER UDARA MIO', 52000, 1, 52000),
(62, '2020-01-19', '200100005', 'BR-0001', 'OLI FEDERAL MATIC 1L', 40000, 2, 80000),
(63, '2020-01-24', '200100006', 'BR-0001', 'OLI FEDERAL MATIC 1L', 40000, 2, 80000),
(64, '2020-09-24', '200900007', 'BR-0001', 'OLI FEDERAL MATIC 1L', 40000, 2, 80000),
(65, '2020-09-25', '200900008', 'BR-0004', 'TUTUP KLEEP VEGA', 23000, 2, 46000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_stokbarang`
--

CREATE TABLE `tb_stokbarang` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `jumlah_stok` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_stokbarang`
--

INSERT INTO `tb_stokbarang` (`id`, `kode_barang`, `jumlah_stok`) VALUES
(1, 'BR-0001', 126),
(2, 'BR-0002', 61),
(3, 'BR-0003', 22),
(4, 'BR-0004', 24),
(5, 'BR-0005', 4),
(6, 'BR-0006', 5),
(7, 'BR-0007', 12),
(8, 'BR-0008', 6),
(9, 'BR-0009', 20),
(10, 'BR-0010', 5),
(11, 'BR-0011', 5),
(12, 'BR-0017', 10),
(14, 'BR-0014', 5),
(15, 'BR-0018', 5),
(16, 'BR-0012', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'admin', '123', '123', 'admin'),
(15, 'anes', 'anes', '123', 'karyawan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indeks untuk tabel `tb_detail_service`
--
ALTER TABLE `tb_detail_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_faktur`
--
ALTER TABLE `tb_faktur`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_faktur_service`
--
ALTER TABLE `tb_faktur_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jenis_service`
--
ALTER TABLE `tb_jenis_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_mekanik`
--
ALTER TABLE `tb_mekanik`
  ADD PRIMARY KEY (`id_mekanik`);

--
-- Indeks untuk tabel `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_stokbarang`
--
ALTER TABLE `tb_stokbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_detail_service`
--
ALTER TABLE `tb_detail_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT untuk tabel `tb_faktur`
--
ALTER TABLE `tb_faktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_faktur_service`
--
ALTER TABLE `tb_faktur_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `tb_jenis_service`
--
ALTER TABLE `tb_jenis_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT untuk tabel `tb_stokbarang`
--
ALTER TABLE `tb_stokbarang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
