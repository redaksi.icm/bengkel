
<?php require('pdf/fpdf.php');?>
<?php
ob_start();
$pdf = new FPDF('l','mm','A5');
$pdf->addPage();
$pdf->SetFont('Times','B',16);
$pdf->Cell(190,5,'LAPORAN SERVICE',0,1,'C');
$pdf->Cell(10,3,'',0,1);
$pdf->SetFont('Times','B',12);
$pdf->Cell(190,5,'Bengkel Anes Motor',0,1, 'C');
$pdf->SetFont('Times','B',10);
$pdf->Cell(190,7,'======================================================================================================',0,1, 'C');
function hari_ini(){
	$hari=date("D");
	switch ($hari) {
			case 'Sun':
			$hasil_ini="Minggu";
			break;
			case 'Mon':
			$hasil_ini="Senin";
			break;
			case 'Tue':
			$hasil_ini="Selasa";
			break;
			case 'Wed':
			$hasil_ini="Rabu";
			break;
			case 'Thu':
			$hasil_ini="Kamis";
			break;
			case 'Fri':
			$hasil_ini="Jumat";
			break;
			case 'Sat':
			$hasil_ini="Sabtu";
			break;
		
		default:
			$hasil_ini="Tidak ada";
			break;
	}
	return $hasil_ini;
}
$h=date("D");
$tgl_now=date('d-m-Y  h:i');
$pdf->SetFont('TIMES', '', 10);
$pdf->Ln(0.5);
$pdf->Cell(0, 1,'Tanggal'.' :'.' '.hari_ini($h).', '.' '.$tgl_now, 0, 0, 'C');
$pdf->SetFont('TIMES', 'B', 14);
$pdf->Ln(5);
$pdf->Cell(0, 10,'SERVICE', 0, 0, 'C');
$pdf->Cell(10,12,'',0,1);
$pdf->SetFont('Times','B',8);
$pdf->Cell(10,8,'NO',1,0);
$pdf->Cell(20,8,'Tanggal',1,0);
$pdf->Cell(30,8,'No Service',1,0);
$pdf->Cell(30,8,'Jenis Service',1,0);
$pdf->Cell(30,8,'Nama Pelanggan',1,0);
$pdf->Cell(35,8,'Nama Mekanik',1,0);
$pdf->Cell(35,8,'Biaya',1,0);
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('TIMES', '', 8);
$no=1;
include "../../koneksi.php";
$sql=mysqli_query($koneksi,"SELECT tb_faktur_service.status,tb_faktur_service.id,tb_faktur_service.no_service,tb_faktur_service.tanggal,tb_faktur_service.jenis_service,tb_faktur_service.id_pelanggan,tb_faktur_service.nama_mekanik,tb_jenis_service.jenis_service,tb_jenis_service.biaya FROM   tb_faktur_service INNER JOIN tb_jenis_service ON tb_faktur_service.jenis_service=tb_jenis_service.jenis_service");
 while ($tampil=mysqli_fetch_array($sql)) {
 	$biaya[ ]=$tampil['biaya'];
 	$hasil1=array_sum($biaya);
$pdf->SetFont('TIMES', '', 8);
$pdf->Cell(10,7,$no,1,0,'C');
$pdf->Cell(20,7,$tampil['tanggal'],1,0);
$pdf->Cell(30,7,$tampil['no_service'],1,0);
$pdf->Cell(30,7,$tampil['jenis_service'],1,0);
$pdf->Cell(30,7,strtoupper($tampil['id_pelanggan']),1,0);
$pdf->Cell(35,7,strtoupper($tampil['nama_mekanik']),1,0);
$pdf->Cell(35,7,rupiah($tampil['biaya']),1,0);
$pdf->Cell(10,7,'',0,1);


$no++;
}   
$pdf->SetFont('TIMES', 'B', 9);   
$pdf->Cell(10,7,'',1,0,'C');
$pdf->Cell(20,7,$tampil['tanggal'],1,0);
$pdf->Cell(30,7,$tampil['no_service'],1,0);
$pdf->Cell(30,7,$tampil['jenis_service'],1,0);
$pdf->Cell(30,7,strtoupper($tampil['id_pelanggan']),1,0);
$pdf->Cell(35,7,' Total ',1,0);
$pdf->Cell(35,7,rupiah($hasil1),1,0);
$pdf->Cell(10,7,'',0,1);
                
$pdf->Cell(10,7,'',0,1);
// akhir service

// barang terjual
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('TIMES', 'B', 14);
$pdf->Cell(0, 1,'BARANG YANG TERJUAL', 0, 0, 'C');
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','B',8);
$pdf->Cell(10,8,'NO',1,0);
$pdf->Cell(20,8,'Tanggal',1,0);
$pdf->Cell(20,8,'No Service',1,0);
$pdf->Cell(25,8,'Jenis Service',1,0);
$pdf->Cell(30,8,'Nama Pelanggan',1,0);
$pdf->Cell(43,8,'Nama Barang',1,0);
$pdf->Cell(12,8,'Jumlah ',1,0);
$pdf->Cell(30,8,'Harga Total',1,0);
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','',8);
include "../../koneksi.php";
if (empty($_POST['tahun'])) {
	echo "<script>
		alert('Upss!! Isi Tahun dan Bulan Boskuu!!');location='../index.php?p=laporan_service';
	</script>";
	
}  elseif (empty($_POST['bulan'])) {
	echo "<script>
		alert('Upss!! Isi Tahun dan Bulan Boskuu!!');location='../index.php?p=laporan_service';
	</script>";
} else{
	$tahun=$_POST['tahun'];
	$bulan=$_POST['bulan'];


}           
function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp. " . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
 $sql=mysqli_query($koneksi,"SELECT * FROM tb_detail_service where year(tgl)='$tahun' AND month(tgl)='$bulan'");
 $cek=mysqli_fetch_array($sql);
 if (empty($cek)) {
 	echo "<script>
		alert('Penjualan Di Tahun Dan Bulan Tersebut Kosong!');location='../index.php?p=laporan_service';
	</script>";
 }

$no=1;
 include "../../koneksi.php";
                   
$sql=mysqli_query($koneksi,"SELECT tb_detail_service.tgl,tb_detail_service.no_service,tb_detail_service.nama_pelanggan,tb_detail_service.nama_mekanik,tb_detail_service.nama_barang,tb_detail_service.jumlah,tb_detail_service.total_harga,tb_faktur_service.jenis_service FROM   tb_detail_service LEFT JOIN tb_faktur_service ON tb_detail_service.no_service=tb_faktur_service.no_service where year(tb_detail_service.tgl)='$tahun' AND month(tb_detail_service.tgl)='$bulan'");
while ($tampil=mysqli_fetch_array($sql)) {
	$total []=$tampil['total_harga'];
	$hasil= array_sum($total);

$pdf->Cell(10,7,$no,1,0,'C');
$pdf->Cell(20,7,$tampil['tgl'],1,0);
$pdf->Cell(20,7,$tampil['no_service'],1,0);
$pdf->Cell(25,7,$tampil['jenis_service'],1,0);
$pdf->Cell(30,7,strtoupper($tampil['nama_pelanggan']),1,0);
$pdf->Cell(43,7,$tampil['nama_barang'],1,0);
$pdf->Cell(12,7,$tampil['jumlah'],1,0);
$pdf->Cell(30,7,rupiah($tampil['total_harga']),1,0);
$pdf->Cell(10,7,'',0,1);

$no++;
}	
$pdf->SetFont('TIMES', 'B', 10);
$pdf->Cell(10,7,'',1,0,'C');
$pdf->Cell(20,7,'',1,0);
$pdf->Cell(20,7,'',1,0);
$pdf->Cell(25,7,'',1,0);
$pdf->Cell(30,7,'',1,0);
$pdf->Cell(43,7,'Total',1,0);
$pdf->Cell(0.05,7,'',0,0);
$pdf->Cell(42,7,rupiah($hasil),1,0);
$pdf->Cell(10,7,'',0,1);
$pdf->Output();
ob_end_flush(); 
?>