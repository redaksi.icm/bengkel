
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Input Jenis Service</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form class="form-horizontal" action="tmbh_jenis_service.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Service</label>

                  <div class="col-sm-10">
                    <input  required=""  name="nama_service" type="text" class="form-control"  placeholder="Jenis Service">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Harga Pelayanan</label>

                  <div class="col-sm-10">
                    <input  required=""  name="harga_p" type="text" class="form-control"  placeholder="Harga">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
            <button type="submit" class="btn btn-default">Cancel</button>
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>

              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Data Service</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10px">No</th>
                  <th width="30px">Jenis Service</th>
                  <th>Biaya</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_jenis_service");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['jenis_service']; ?></td>
                  <td><?= rupiah($view['biaya']); ?></td>
                  
                  <td>
                    <a class="btn btn-info" data-toggle="modal" data-target="#modal<?= $view['id'];  ?>" ><i class="fa fa-pencil"></i>
                    Edit
                  </a>
          <div class="modal fade" id="modal<?= $view['id'];  ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Jenis Service</h4>
              </div>
              <form class="" action="edit/edit_jenis_service.php" method="post">
              <div class="modal-body pull-center">
                 <input  name="id" hidden=""  value="<?= $view['id'];  ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Jenis Service</label>

                  <div class="col-sm-6">
                    <input  value="<?= $view['jenis_service'];  ?>" required=""  type="text" class="form-control" name="jenis"  placeholder="Nama Barang">
                  </div>
                </div>
                <br>
              </div>
            <div class="modal-body">
                <div class="form-group pull-center">
                  <label  for="inputEmail3" class="col-sm-4 control-label">Biaya Service</label>

                  <div class="col-sm-8">
                    <input id="rupiah" value="<?= $view['biaya'];  ?>" required=""  type="text" class="form-control" name="biaya"  placeholder="ID">
                  </div>
                </div>
                <br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" type="button" class="btn btn-primary">Save changes</button>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
                  
                  <a href="index.php?p=hapus_jenis_service&hapus_jenis_service=<?= $view['id']; ?>">
                    <button onclick="return confirm('Anda akan menghapus')" name="edit" type="button" class="btn btn-danger">
                    <i class="fa fa-times-circle"></i></button>
                  </a>
                  </td>
                </tr>
              <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>