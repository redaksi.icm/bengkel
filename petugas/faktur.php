
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah No Faktur</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php 
            include "../koneksi.php";
            $query="SELECT max(no_faktur) as maxKode FROM tb_faktur";
            $hasil=mysqli_query($koneksi,$query);
            $data=mysqli_fetch_array($hasil);
            $kode_barang=$data['maxKode'];

            $no=(int)substr($kode_barang, 5, 5);
            $no++;

            $t=date('y');
            $m=date('m');

            $kode_penjualan= $t . $m. sprintf("%05s", $no);

            $tanggal=date('l, d-m-y');

             ?>
            <form class="form-horizontal" action="tambah_faktur.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NO Faktur</label>

                  <div class="col-sm-6">
                    <input id="no" readonly="" value="<?= $kode_penjualan ?>"  type="text" class="form-control" name="no" >
                  </div>
                </div>
                </div>
              <div class="box-footer">
            <button style="margin-left: 256px" value="tambah" name="tambah" type="submit" class="btn btn-info pull-center">Tambah</button>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-6">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Faktur</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Faktur</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
              
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_faktur ORDER BY id DESC");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                    
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['no_faktur']; ?></td>
                  <td><?= $view['tanggal']; ?></td>
                  <td>
                  <?php if ($view['status']=='y'): ?>
                    <a  href="index.php?p=isi_barang&isi_faktur=<?= $view['no_faktur']; ?>">
                    <button disabled="" name="tambah" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i></button>
                  </a>
                  <?php endif ?>
                  <?php if ($view['status']=='x'): ?>
                    
                  <a  href="index.php?p=isi_barang&isi_faktur=<?= $view['no_faktur']; ?>">
                    <button name="tambah" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i></button>
                  </a>
                  <?php endif ?>

                  <?php if ($view['status']=='y'): ?>
                  <a  href="index.php?p=hapus_faktur&hapus_faktur=<?= $view['id']; ?>">
                    <button disabled="" name="edit" type="button" class="btn btn-danger">
                    <i class=""></i>Hapus</button>
                  </a>
                  <?php endif ?>
                  <?php if ($view['status']=='x'): ?>
                  <a  href="index.php?p=hapus_faktur&hapus_faktur=<?= $view['id']; ?>">
                    <button onclick=" return confirm('Anda Akan menghapus')"   name="edit" type="button" class="btn btn-danger">
                    <i class=""></i>Hapus</button>
                  </a>
                  <?php endif ?>   
                  
                    <a onclick=" return confirm('Penting!! Nota Akan di print, Anda tidak bisa melakukan perubahan lagi!')"  href="cetak/print_struk_jual.php?print_faktur=<?= $view['no_faktur']; ?>&faktur_id=<?= $view['id']; ?>">
                    <button name="edit" type="button" class="btn btn-info">
                    <i class="fa fa-print"></i></button>
                  </a>
                  </td>
                </tr>
              <?php $no++; } ?>
              
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <script type="text/javascript">
            <?php echo $jsArray; ?>
            function changeValue(id){
            document.getElementById('harga_jual') .value= prdName[id].harga_jual;
            
          };
         
          </script>
