<div class="col-md-12 pull-center">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Nota Service</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php
    include "../koneksi.php";
    $query = "SELECT max(no_service) as maxKode FROM tb_faktur_service";
    $hasil = mysqli_query($koneksi, $query);
    $data = mysqli_fetch_array($hasil);
    $kode_barang = $data['maxKode'];

    $no = (int)substr($kode_barang, 5, 5);
    $no++;
    $h = "SRV";

    $kode_penjualan = $h . sprintf("%05s", $no);

    $tanggal = date('l, d-m-y');

    ?>
    <form class="form-horizontal" action="tambah_transaksi_service.php" method="post">
      <div class="box-body">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">No Service</label>

          <div class="col-sm-5">
            <input id="no_service" readonly="" value="<?= $kode_penjualan ?>" type="text" class="form-control" name="no_service">
          </div>
        </div>
        <div class="form-group">

          <label class="col-sm-3 control-label">Jenis Service</label>
          <div class="col-sm-5">
            <select required="" class="form-control" id="jenis_service" name="jenis_service" onchange="changeValue(this.value)">
              <option disabled="" selected="">Pilih</option>
              <?php
              $sql = mysqli_query($koneksi, "SELECT * FROM tb_jenis_service");
              while ($data = mysqli_fetch_array($sql)) {

                echo '<option value="' . $data['id'] . '">' . $data['jenis_service'] . '</option> ';
              }
              ?>
            </select>

          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Nama Pelanggan</label>

          <div class="col-sm-5">
            <select required="" class="form-control" id="nama_pelanggan" name="nama_pelanggan" onchange="changeValue(this.value)">
              <option disabled="" selected="">Pilih</option>
              <?php
              $sql = mysqli_query($koneksi, "SELECT * FROM tb_pelanggan");
              while ($data = mysqli_fetch_array($sql)) {

                echo '<option value="' . $data['nama'] . '">' . $data['nama'] . ' ' . $data['no_kendaraan'] . '</option> ';
              }
              ?>
            </select>

          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Nama Mekanik</label>

          <div class="col-sm-5">
            <select required="" class="form-control" id="nama_mekanik" name="nama_mekanik" onchange="changeValue(this.value)">
              <option disabled="" selected="">Pilih</option>
              <?php
              $sql = mysqli_query($koneksi, "SELECT * FROM tb_mekanik");
              while ($data = mysqli_fetch_array($sql)) {

                echo '<option value="' . $data['nama_mekanik'] . '">' . $data['nama_mekanik'] . '</option> ';
              }
              ?>
            </select>

          </div>
        </div>
      </div>
      <div class="box-footer">
        <button style="margin-left: 600px" value="tambah" name="tambah" type="submit" class="btn btn-info ">Tambah</button>
      </div>
      <!-- /.box-body -->
    </form>
  </div>
  <!-- /.box -->
</div>

<div class="col-md-12">

  <div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Tabel Nota Service</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="10px">No</th>
            <th width="10px">No Service</th>
            <th>Tanggal</th>
            <th>Jenis Service</th>
            <th width="20px">Nama Pelanggan</th>
            <th>Nama Mekanik</th>
            <th>Biaya</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>

          <?php
          function rupiah($angka)
          {
            $hasil_rupiah = "Rp" . number_format($angka, 2, ',', '.');
            return $hasil_rupiah;
          }
          ?>
          <?php
          include "../koneksi.php";

          if (isset($_GET['total'])) {
            $t = $_GET['total'];
            $h = $t;
          } else {
            $h = 0;
          }


          $no = 1;


          $sql = mysqli_query($koneksi, "SELECT tb_faktur_service.status,tb_faktur_service.id,tb_faktur_service.no_service,tb_faktur_service.tanggal,tb_faktur_service.jenis_service,tb_faktur_service.id_pelanggan,tb_faktur_service.nama_mekanik,tb_jenis_service.jenis_service,tb_jenis_service.biaya FROM   tb_faktur_service LEFT JOIN tb_jenis_service ON tb_faktur_service.jenis_service=tb_jenis_service.jenis_service ORDER BY tb_faktur_service.id DESC");
          while ($view = mysqli_fetch_array($sql)) {



          ?>

            <tr>
              <td><?= $no; ?></td>
              <td><?= $view['no_service']; ?></td>
              <td><?= $view['tanggal']; ?></td>
              <td><?= $view['jenis_service']; ?></td>
              <td><?= $view['id_pelanggan']; ?></td>
              <td><?= $view['nama_mekanik']; ?></td>
              <td><?= rupiah($view['biaya'] + $h); ?></td>
              <td><?php
                  if ($view['status'] == 'x') {
                    echo 'Pending';
                  } elseif ($view['status'] == 'k') {
                    echo 'Dikerjakan';
                  } elseif ($view['status'] == 'y') {
                    echo 'Selesai';
                  }
                  ?></td>


              <td>
                <a href="index.php?p=barang_service&barang_service=<?= $view['id']; ?>">
                  <button name="edit" type="button" class="btn btn-info">
                    <i class="fa fa-plus"></i></button>
                </a>
                <?php if ($view['status'] == 'y') : ?>
                  <a onclick="return confirm('Anda akan print struk service')" target="_blank" href="cetak/print_struk_service.php?cetak_struk=<?= $view['id']; ?>">
                    <button name="edit" type="button" class="btn btn-success">
                      <i class="fa fa-print"></i></button>
                  </a>
                <?php endif ?>
                <?php if ($view['status'] == 'k') : ?>

                  <a href="index.php?p=selesaikan&id=<?= $view['id']; ?>">
                    <button name="edit" type="button" class="btn btn-yellow" style="background: #2f2a28;color: #f7f7ef;">
                      <i></i>Selesaikan</button>
                  </a>
                <?php endif ?>
                <?php if ($view['status'] == 'x') : ?>

                  <a href="index.php?p=kerjakan&id=<?= $view['id']; ?>">
                    <button name="edit" type="button" class="btn btn-warning">
                      <i></i>Kerjakan</button>
                  </a>
                <?php endif ?>
                <?php if ($view['status'] == 'x') : ?>

                  <a href="index.php?p=hapus_isi_service&hapus_isi_service=<?= $view['id']; ?>">
                    <button name="edit" type="button" class="btn btn-danger">
                      <i></i>Hapus</button>
                  </a>
                <?php endif ?>
              </td>



            </tr>
          <?php $no++;
          } ?>

        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <script type="text/javascript">
    <?php echo $jsArray; ?>

    function changeValue(id) {
      document.getElementById('harga_jual').value = prdName[id].harga_jual;

    };
  </script>