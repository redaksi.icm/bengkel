<script src="jquery.js"></script>
<?php 
include "../koneksi.php";

if ($_GET['isi_faktur']) {
	$tgl=date('y-m-d');
	$no_faktur=$_GET['isi_faktur'];
}
 ?>

        <div class="col-md-7">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Penjualan</h3>
            </div>
            <form class="form-horizontal" action="" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NO Faktur</label>

                  <div class="col-sm-6">
                    <input id="no" readonly="" value="<?= $no_faktur; ?>"  type="text" class="form-control" name="no" >
                    <p>*No faktur</p>
                  </div>
                </div>
                <div class="form-group">
                	<input value="<?= $no_faktur; ?>" type="hidden" name="no_faktur">
                  <label class="col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-6">
                    <select required="" class="form-control" id="nama" name="nama" onchange="changeValue(this.value)">
                       <option disabled=""  selected="" >Pilih</option>
                       <?php 
                         $sql=mysqli_query($koneksi,"SELECT  tb_barang.harga_jual,tb_barang.kode_barang,tb_barang.nama_barang,tb_stokbarang.kode_barang,tb_stokbarang.jumlah_stok from tb_barang INNER JOIN tb_stokbarang ON tb_stokbarang.kode_barang=tb_barang.kode_barang");
                         $jsArray = "var prdName = new Array();\n"; 
                        
                        while($data=mysqli_fetch_array($sql)){
                          if (empty($data['jumlah_stok'])) {
                          }else{
                               
                          echo '<option value="'.$data['nama_barang'].'">'.$data['nama_barang'].'</option> ';
                          $jsArray .= "prdName['" . $data['nama_barang'] . "'] = {harga_jual:'" . addslashes($data['harga_jual']). "'};\n";
                            
                        
                          }
                         }
                        ?>
                      </select>

                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Harga/satuan</label>

                  <div class="col-sm-6">
                    <input readonly=""  id="harga_jual"   required=""  type="text" class="form-control" name="harga_jual"  placeholder="">
                  </div>
                
              </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jumlah</label>
                  <div class="col-sm-6">
                  <input required=""  type="text" class="form-control" name="jumlah"  placeholder="Jumlah">
                  </div> 
                </div>
          <div class="box-footer">
            <a href="">
          <button value="tambah" name="tambah" class="btn btn-info pull-right">Tambah</button>
        </a>
            </form>
            <a class="btn btn-success" href="index.php?p=penjualan">
                Selesai
              </a>
              </div>
                </div>
          </div>
        </div>
        <div class="col-md-5 pull-right">
          <!-- Horizontal Form -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Sub Total</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <?php 
              include "../koneksi.php";
              $sql=mysqli_query($koneksi,"select sum(jumlah) as jumlah_total from tb_penjualan where no_faktur='$no_faktur'");
              $data=mysqli_fetch_array($sql);
              $jumlah=$data['jumlah_total'];
               ?>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Jumlah barang</label>

                  <div class="col-sm-10">
                    <input readonly="" value="<?= $jumlah; ?>"  class="form-control"  placeholder="">
                  </div>
                </div>
            <?php 
            include "../koneksi.php";
            $sql=mysqli_query($koneksi,"select sum(total_harga) as total from tb_penjualan where no_faktur='$no_faktur'");
            $data=mysqli_fetch_array($sql);
            $jumlah_total=$data['total'];
             ?>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Sub Total</label>

                  <div class="col-sm-10">
                    <input readonly="" value="<?= rupiah($jumlah_total);  ?>"  class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          <!-- /.box -->
        </div>

<?php 
     
include '../koneksi.php'; 
// menangkap data yang di kirim dari form
if (isset($_POST['tambah'])) {


 $no_faktur=$_POST['no_faktur'];
 $nama=$_POST['nama'];
 $jumlah=$_POST['jumlah'];
 $jual=$_POST['harga_jual'];
 $sql=mysqli_query($koneksi,"select * from tb_barang where nama_barang='$nama'");
 $data=mysqli_fetch_array($sql);
 $kode_b=$data['kode_barang'];
 $total=$jumlah * $jual;

$tgl=date('Y-m-d');




// menginput data ke database
mysqli_query($koneksi,"UPDATE tb_faktur set status='y' where no_faktur='$no_faktur'");

  $sql=mysqli_query($koneksi,"select * from tb_stokbarang");
  $data=mysqli_fetch_array($sql);
    if ($data['kode_barang']=$no_faktur) {
      $stok=$data['jumlah_stok'];
      $hasil=$stok-$jumlah;
  if ($_POST['jumlah'] > $data['jumlah_stok'] ) {
      echo "<script>
            alert('Upss! Stok tidak cukup boskuu!');location='index.php?p=isi_barang&isi_faktur=$no_faktur';
            </script>";
    }else{
        mysqli_query($koneksi,"insert into tb_penjualan values('','$tgl','$no_faktur','$kode_b','$nama','$jual','$jumlah','$total')");
        mysqli_query($koneksi,"UPDATE tb_stokbarang set jumlah_stok='$hasil' where kode_barang='$kode_b'");

         echo "<script>
            alert('Berhasil');location='index.php?p=isi_barang&isi_faktur=$no_faktur';
            </script>";
    }
  }

}
?>

        <div class="col-md-12">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Detail Pmbelian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th>Tanggal</th>
                  <th>No Faktur</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Satuan</th>
                  <th>Jumlah</th>
                  <th>Total Harga</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                   
                    $sql=mysqli_query($koneksi,"SELECT tb_faktur.no_faktur,tb_faktur.tanggal , tb_penjualan.kode_barang,tb_penjualan.nama_barang,tb_penjualan.harga,tb_penjualan.jumlah,tb_penjualan.total_harga,tb_penjualan.id from tb_faktur INNER JOIN tb_penjualan ON tb_penjualan.no_faktur=tb_faktur.no_faktur");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                  <?php if ($view['no_faktur']==$no_faktur): ?>
                  	
                <tr>
                  
                  <td><?= $view['tanggal']; ?></td>
                  <td><?= $view['no_faktur']; ?></td>
                  <td><?= $view['kode_barang']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= $view['harga']; ?></td>
                  <td><?= $view['jumlah']; ?></td>
                  <td><?= $view['total_harga']; ?></td>
                  <td>
                    <a href="index.php?p=hapus_pilihan&hapus_pilihan=<?= $view['id']  ?>&no_faktur=<?= $view['no_faktur'] ?>&jumlah=<?= $view['jumlah'] ?>&kode=<?= $view['kode_barang'];  ?>">
                   <button onclick="return confirm('Yakin untuk menghapus')" type="button" class="btn btn-danger" id="hapus"><i></i>hapus</button>
                 </a>
                 </td>
                  </tr>
                  <?php endif ?>
              <?php  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
 
 <script type="text/javascript">
  <?php echo $jsArray; ?>
 function changeValue(id){
 document.getElementById('harga_jual') .value= prdName[id].harga_jual;
            
 };
</script>