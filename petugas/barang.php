
<div class="col-md-5">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php 
            include "../koneksi.php";
            $query="SELECT max(kode_barang) as maxKode FROM tb_barang";
            $hasil=mysqli_query($koneksi,$query);
            $data=mysqli_fetch_array($hasil);
            $kode_barang=$data['maxKode'];

            $no=(int)substr($kode_barang, 4, 4);
            $no++;

            $char="BR-";
            $kode_barang= $char . sprintf("%04s", $no);

             ?>
            <form class="form-horizontal" action="tambah_barang.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kode Barang</label>

                  <div class="col-sm-10">
                    <input readonly="" value="<?= $kode_barang; ?>"  type="text" class="form-control" name="kode_brg">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>

                  <div class="col-sm-10">
                    <input required=""  type="" class="form-control" name="nama_brg"  placeholder="Nama">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Harga Beli</label>

                  <div class="col-sm-10">
                    <input id="rupiah"  required=""  name="harga_beli" type="text" class="form-control"  placeholder="Harga Beli">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="inputPassword3" class="col-sm-2 control-label">Harga Jual</label>

                  <div class="col-sm-10">
                    <input id="rupiah"  required=""  name="harga_jual" type="text" class="form-control"  placeholder="Harga Jual">
                  </div>
                </div>
              </div>

              <!-- /.box-body -->
              <div class="box-footer">
            <button type="submit" class="btn btn-default">Cancel</button>
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-7">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Data Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="8px">No</th>
                  <th width="20px">Kode Barang</th>
                  <th width="20px">Nama Barang</th>
                  <th>Harga Beli</th>
                  <th>Harga Jual</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_barang");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['kode_barang']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= rupiah($view['harga_beli']); ?></td>
                  <td><?= rupiah($view['harga_jual']); ?></td>
                  <td>
                  <a class="btn btn-info" data-toggle="modal" data-target="#modal<?= $view['kode_barang'];  ?>" ><i class="fa fa-pencil"></i>
                    Edit
                  </a>
          <div class="modal fade" id="modal<?= $view['kode_barang'];  ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data Barang</h4>
              </div>
              <form class="" action="edit/edit_data_barang.php" method="post">
              <div class="modal-body pull-center">
                 <input  name="kode_barang" hidden=""  value="<?= $view['kode_barang'];  ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nama Barang</label>

                  <div class="col-sm-6">
                    <input  value="<?= $view['nama_barang'];  ?>" required=""  type="text" class="form-control" name="nama_barang"  placeholder="Nama Barang">
                  </div>
                </div>
                <br>
              </div>
            <div class="modal-body">
                <div class="form-group pull-center">
                  <label  for="inputEmail3" class="col-sm-4 control-label">Harga Beli</label>

                  <div class="col-sm-8">
                    <input id="rupiah" value="<?= $view['harga_beli'];  ?>" required=""  type="text" class="form-control" name="harga_beli"  placeholder="ID">
                  </div>
                </div>
                <br>
              </div>
              <div class="modal-body">
                <div class="form-group pull-center">
                  <label for="inputEmail3" class="col-sm-4 control-label">Harga Jual</label>

                  <div class="col-sm-8">
                    <input id="rupiah" value="<?= $view['harga_jual'];  ?>" required=""  type="text" class="form-control" name="harga_jual"  placeholder="ID">
                  </div>
                </div>
                <br>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" type="button" class="btn btn-primary">Save changes</button>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
                  <a href="index.php?p=hapus_brg&hapus_barang=<?= $view['kode_barang']; ?>">
                    <button onclick="return confirm('Yakin Untuk Menghapus Item <?= $view['kode_barang']  ?>')" name="edit" type="button" class="btn btn-danger">
                    <i class="fa fa-times-circle"></i></button>
                  </a>
                  </td>
                </tr>
              <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>