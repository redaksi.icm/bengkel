
<div class="col-md-5">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Pelanggan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <?php 
            include "../koneksi.php";
            $query="SELECT max(id) as maxKode FROM tb_pelanggan";
            $hasil=mysqli_query($koneksi,$query);
            $data=mysqli_fetch_array($hasil);
            $id_pelanggan=$data['maxKode'];

            $no=(int)substr($id_pelanggan, 4, 4);
            $no++;

            $char="PL-";
            $id_pelanggan= $char . sprintf("%04s", $no);

             ?>
            <form class="form-horizontal" action="tambah_pelanggan.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">ID Pelanggan</label>

                  <div class="col-sm-10">
                    <input readonly="" value="<?= $id_pelanggan; ?>" required=""  type="text" class="form-control" name="id_pelanggan"  placeholder="ID">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input required=""  name="nama" type="text" class="form-control"  placeholder="Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="inputPassword3" class="col-sm-2 control-label">No kendaraan</label>

                  <div class="col-sm-10">
                    <input style="text-transform: uppercase;" required=""  name="no" type="text" class="form-control"  placeholder="baxxx..">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
            <button type="submit" class="btn btn-default">Cancel</button>
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>

              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-7">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Data Pelanggan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20px">No</th>
                  <th>ID Pelanggan</th>
                  <th>Nama</th>
                  <th>No Kendaraan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_pelanggan");
                    while ($view=mysqli_fetch_array($sql)) {
                    $id=$view['id'];
                   ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['id']; ?></td>
                  <td><?= $view['nama']; ?></td>
                  <td><?= $view['no_kendaraan']; ?></td>
                  <td>
                  <a class="btn btn-info" data-toggle="modal" data-target="#modal<?= $view['id'];  ?>" ><i class="fa fa-pencil"></i>
                    Edit
                  </a>
          <div class="modal fade" id="modal<?= $view['id'];  ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data Pelanggan</h4>
              </div>
              <form class="" action="edit/edit_pelanggan.php" method="post">
              <div class="modal-body pull-center">
                 <input name="id" hidden=""  value="<?= $view['id'];  ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nama Pelanggan</label>

                  <div class="col-sm-6">
                    <input  value="<?= $view['nama'];  ?>" required=""  type="text" class="form-control" name="nama"  placeholder="Nama Pelanggan">
                  </div>
                </div>
                <br>
              </div>
            <div class="modal-body">
                <div class="form-group pull-center">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nomor Kendaraan</label>

                  <div class="col-sm-8">
                    <input  value="<?= $view['no_kendaraan'];  ?>" required=""  type="text" class="form-control" name="no"  placeholder="ID">
                  </div>
                </div>
                <br>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" type="button" class="btn btn-primary">Save changes</button>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
                  <a href="index.php?p=hapus_p&hapus_p=<?= $view['id']; ?>">
                    <button onclick="return confirm('Yakin Untuk Menghapus Pelanggan <?= $view['nama']  ?>')" name="edit" type="button" class="btn btn-danger">
                    <i class="fa fa-times-circle"></i></button>
                  </a>
                  </td>
                </tr>
              <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>