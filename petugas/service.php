<script src="jquery.js"></script>

<div class="col-md-6">
          <!-- Horizontal Form -->
           <?php 
            include "../koneksi.php";
            $query="SELECT max(no_faktur) as maxKode FROM tb_faktur";
            $hasil=mysqli_query($koneksi,$query);
            $data=mysqli_fetch_array($hasil);
            $kode_barang=$data['maxKode'];

            $no=(int)substr($kode_barang, 5, 5);
            $no++;
            $h='SRV';
            $t=date('y');
            $m=date('m');

            $kode_service=$h. $t . $m. sprintf("%05s", $no);

            $tanggal=date('l, d-m-y');

             ?>
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NO SERVICE</h3>
            </div>
            <form class="form-horizontal" action="tambah_faktur.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">NO Faktur</label>

                  <div class="col-sm-6">
                    <input id="no" readonly="" value="<?= $kode_service; ?>"  type="text" class="form-control" name="no" >
                    <p>*No Service</p>
                  </div>
                </div>
                </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Barang Service</h3>
            </div>
            <form class="form-horizontal" action="" method="post">
              <div class="box-body">
                <div class="form-group">
                	<input value="<?= $no_faktur; ?>" type="hidden" name="no_faktur">
                  <label class="col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-6">
                    <select required="" class="form-control" id="nama" name="nama" onchange="changeValue(this.value)">
                       <option disabled=""  selected="" >Pilih</option>
                       <?php 
                         $sql=mysqli_query($koneksi,"SELECT * FROM tb_barang");
                         $jsArray = "var prdName = new Array();\n"; 
                        
                         while ($data=mysqli_fetch_array($sql)) {
                        
                          echo '<option value="'.$data['nama_barang'].'">'.$data['nama_barang'].'</option> ';
                          $jsArray .= "prdName['" . $data['nama_barang'] . "'] = {harga_jual:'" . addslashes($data['harga_jual']). "'};\n";
                        
                         }
                        ?>
                      </select>

                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Harga/satuan</label>

                  <div class="col-sm-6">
                    <input readonly=""  id="harga_jual"   required=""  type="text" class="form-control" name="harga_jual"  placeholder="">
                  </div>
                
              </div>
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jumlah</label>
                  <div class="col-sm-6">
                  <input required=""  type="text" class="form-control" name="jumlah"  placeholder="Jumlah">
                  </div>
                  
                </div>
                <div class="box-footer">
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
                	<a href="">
                		<button type="submit" class="btn btn-danger pull-center">Cetak</button>
                	</a>
              </div>
                </div>
            </form>
          </div>
        </div>
        <?php 
// koneksi database
       if (isset($_POST['tambah'])) {
       	# code...
       
include '../koneksi.php';
 
// menangkap data yang di kirim dari form
if ($_POST['tambah']) {


 $no_faktur=$_POST['no_service'];
 $nama=$_POST['nama'];
 $jumlah=$_POST['jumlah'];
 $jual=$_POST['harga_jual'];
 $sql=mysqli_query($koneksi,"select * from tb_barang where nama_barang='$nama'");
 $data=mysqli_fetch_array($sql);
$kode_b=$data['kode_barang'];
$total=$jumlah * $jual;

$tgl=date('l,y-m-d');

// menginput data ke database
if ($jumlah < 0) {
	echo "<script>alert('masukan jumlah');";
}else
mysqli_query($koneksi,"UPDATE tb_faktur set status='y' where no_faktur='$no_faktur'");
mysqli_query($koneksi,"insert into tb_penjualan values('','$no_faktur','$kode_b','$nama','$jual','$jumlah','$total')");
 }
}
?>

        <div class="col-md-12">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Detail Pmbelian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th>Tanggal</th>
                  <th>No Faktur</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Satuan</th>
                  <th>Jumlah</th>
                  <th>Total Harga</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                   
                    $sql=mysqli_query($koneksi,"SELECT tb_faktur.no_faktur,tb_faktur.tanggal , tb_penjualan.kode_barang,tb_penjualan.nama_barang,tb_penjualan.harga,tb_penjualan.jumlah,tb_penjualan.total_harga from tb_faktur INNER JOIN tb_penjualan ON tb_penjualan.no_faktur=tb_faktur.no_faktur");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                  <?php if ($view['no_faktur']==$no_faktur): ?>
                  	
                <tr>
                  
                  <td><?= $view['tanggal']; ?></td>
                  <td><?= $view['no_faktur']; ?></td>
                  <td><?= $view['kode_barang']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= $view['harga']; ?></td>
                  <td><?= $view['jumlah']; ?></td>
                  <td><?= $view['total_harga']; ?></td>
                  <td>
                
                  
                                      <button type="button" id="hapus" onclick="hapus('<?= $view['id'] ?>')">hapus</button>
                </tr>
                
                  <?php endif ?>
              <?php  } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <script type="text/javascript">
            <?php echo $jsArray; ?>
            function changeValue(id){
            document.getElementById('harga_jual') .value= prdName[id].harga_jual;
            
          };
         
          </script>