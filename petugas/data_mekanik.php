
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Mekanik</h3>
            </div>
            <!-- /.box-header -->
            <?php 
            include "../koneksi.php";
            $query="SELECT max(id_mekanik) as maxKode FROM tb_mekanik";
            $hasil=mysqli_query($koneksi,$query);
            $data=mysqli_fetch_array($hasil);
            $kode_barang=$data['maxKode'];

            $no=(int)substr($kode_barang, 4, 4);
            $no++;

            $char="MK-";
            $kode_mekanik= $char . sprintf("%04s", $no);

             ?>
            <form class="form-horizontal" action="tambah_mekanik.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">ID</label>

                  <div class="col-sm-10">
                    <input value="<?= $kode_mekanik; ?>" required=""  type="text" class="form-control" name="id_mekanik"  readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input required=""  name="nama_mekanik" type="text" class="form-control"  placeholder="Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="inputPassword3" class="col-sm-2 control-label">No HP</label>

                  <div class="col-sm-10">
                    <input required=""  name="no_hp" type="text" class="form-control"  placeholder="No HP">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
            <button type="submit" class="btn btn-default">Cancel</button>
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>

              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
      <div class="row">
        <div class="col-md-6">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Data Mekanik</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20px">No</th>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>No HP</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_mekanik ");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['id_mekanik']; ?></td>
                  <td><?= $view['nama_mekanik']; ?></td>
                  <td><?= $view['no_hp']; ?></td>
                  <td>
                    <a class="btn btn-info" data-toggle="modal" data-target="#modal<?= $view['id_mekanik'];  ?>" ><i class="fa fa-pencil"></i>
                    Edit
                  </a>
          <div class="modal fade" id="modal<?= $view['id_mekanik'];  ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data Mekanik</h4>
              </div>
              <form class="" action="edit/edit_mekanik.php" method="post">
              <div class="modal-body pull-center">
                 <input name="id" hidden=""  value="<?= $view['id_mekanik'];  ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nama Mekanik</label>

                  <div class="col-sm-6">
                    <input  value="<?= $view['nama_mekanik'];  ?>" required=""  type="text" class="form-control" name="nama"  placeholder="Nama Pelanggan">
                  </div>
                </div>
                <br>
              </div>
            <div class="modal-body">
                <div class="form-group pull-center">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nomor Telepon</label>

                  <div class="col-sm-8">
                    <input  value="<?= $view['no_hp'];  ?>" required=""  type="text" class="form-control" name="no"  placeholder="ID">
                  </div>
                </div>
                <br>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" type="button" class="btn btn-primary">Save changes</button>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
                  <a href="index.php?p=hapus_mekanik&hapus_m=<?= $view['id_mekanik']; ?>">
                    <button onclick="return confirm('Anda Akan Menghapus')" name="edit" type="button" class="btn btn-danger">
                    <i class="fa fa-times-circle"></i></button>
                  </a>
                  </td>
                </tr>
              <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br><br><br><br>
      <!-- /.row -->