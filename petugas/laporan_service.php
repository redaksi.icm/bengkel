        <div class="col-md-12 pull-center">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">From Cetak</h3>
            </div>
            <form class="form-horizontal" action="cetak/cetak_laporan_service.php" method="post">
              <div class="box-body">
                               <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tahun</label>
                  <div class="col-sm-3">
                    <select required="" class="form-control" id="tahun" name="tahun">
                       <option disabled=""  selected="" >Pilih</option>
                       <?php 
                         $tahun_now=date('Y');
                         $tahun=2012;
                         for ($a=2020;$a >=$tahun; $a--) { 
                        ?>
                           <option value="<?php echo $a ?>"><?php echo $a; ?></option> 
                      <?php } ?>
                      </select>

                  </div>
                  
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Bulan Tahun</label>

                  <div class="col-sm-3">
                    <select required="" class="form-control" id="bulan" name="bulan">
                       <option disabled=""  selected="" >Pilih Bulan</option>
                       <option value="01" >Januari</option>
                       <option value="02"  >Februari</option>
                       <option value="03"  >Maret</option>
                       <option value="04" >April</option>
                       <option value="05" >Mei</option>
                       <option value="06" >Juni</option>
                       <option value="07" >Juli</option>
                       <option value="08" >Agustus</option>
                       <option value="09" >September</option>
                       <option value="10" >Oktober</option>
                       <option value="11" >November</option>
                       <option value="12" >Desember</option>
                       
                      </select>

                  </div>
                </div>
          <div class="box-footer">
            <a href="">
          <button value="cetak" name="cetak" type="submit"  class="btn btn-danger pull-right"><i class="fa fa-print"> </i>Cetak PDF</button>
        </a>
            </form>
            <a class="btn btn-default" href="index.php?p=beranda">
                Cencel
              </a>
              </div>
                </div>
          </div>
        </div>
       

     <div class="col-md-12">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Detail Service</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th width="10px">No</th>
                  <th width="10px">No Service</th>
                  <th>Tanggal</th>
                  
                  <th width="20px">Nama Pelanggan</th>
                  <th>Nama Mekanik</th>
                  <th>Nama Barang</th>
                  <th>Jumlah</th>
                  <th>Total</th>
                  
                  
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                   $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM tb_detail_service ");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                  
                    
                <tr>
                  
                  <td><?= $no; ?></td>
                  <td><?= $view['no_service']; ?></td>
                  <td><?= $view['tgl']; ?></td>
                  <td><?= $view['nama_pelanggan']; ?></td>
                  <td><?= $view['nama_mekanik']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= $view['jumlah']; ?></td>
                  <td><?= rupiah( $view['total_harga']); ?></td>
                  
                  </tr>
                  
              <?php $no++;  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
 
 <script type="text/javascript">
  <?php echo $jsArray; ?>
 function changeValue(id){
 document.getElementById('harga_jual') .value= prdName[id].harga_jual;
            
 };
</script>