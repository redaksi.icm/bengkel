<div class="col-md-12">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Detail Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th>NO</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Jual</th>
                  <th>Harga Beli</th>
                  <th>Jumlah Stok</th>

                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                   $no=1;
                    $sql=mysqli_query($koneksi,"SELECT  tb_stokbarang.kode_barang,tb_stokbarang.jumlah_stok,tb_barang.kode_barang,tb_barang.nama_barang FROM tb_stokbarang INNER JOIN tb_barang ON tb_stokbarang.kode_barang=tb_barang.kode_barang");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                  
                <?php if ($view['jumlah_stok'] < 5): ?>
                    
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['kode_barang']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= $view['harga_beli']; ?></td>
                  <td><?= $view['harga_jual']; ?></td>
                  <td style="color: red"><?= $view['jumlah_stok']; ?></td>
                  </tr>
                  <?php endif ?>  
                  
              <?php $no++;  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>