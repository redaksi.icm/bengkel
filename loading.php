<?php

$page = (isset($_GET['p'])) ? $_GET['p'] : '';
switch ($page) {
	case 'beranda':
		include "admin/beranda.php";
		break;
	case 'user':
		include "admin/user.php";
		break;
	case 'hapus':
		include "admin/hapus_user.php";
		break;
	case 'tambah_user':
		include "admin/user_tambah.php";
		break;
	case 'home':
		include "petugas/beranda.php";
		break;
	case 'barang':
		include "petugas/barang.php";
		break;
	case 'hapus_brg':
		include "petugas/hapus_barang.php";
		break;
	case 'stok_barang':
		include "petugas/stok_barang.php";
		break;
		// case 'stok_barang':
		// include "petugas/stok_barang.php";
		// break;
	case 'data_mekanik':
		include "petugas/data_mekanik.php";
		break;
	case 'hapus_stok':
		include "petugas/hapus_stok.php";
		break;
	case 'hapus_mekanik':
		include "petugas/hapus_mekanik.php";
		break;
	case 'penjualan':
		include "petugas/faktur.php";
		break;
	case 'pelanggan':
		include "petugas/pelanggan.php";
		break;
	case 'hapus_p':
		include "petugas/hapus_pelanggan.php";
		break;
	case 'faktur':
		include "petugas/faktur.php";
		break;
	case 'isi_barang':
		include "petugas/isi_barang.php";
		break;
	case 'hapus_isi':
		include "petugas/hapus_isi.php";
		break;
	case 'hapus_faktur':
		include "petugas/hapus_faktur.php";
		break;
	case 'service':
		include "petugas/transaksi_service.php";
		break;
	case 'hapus_service':
		include "petugas/hapus_service.php";
		break;
	case 'barang_service':
		include "petugas/tmbh_brg_service.php";
		break;
	case 'jenis_service':
		include "petugas/jenis_service.php";
		break;
	case 'cetak_srtuk':
		include "petugas/cetak/print_struk_service.php";
		break;
	case 'print_struk':
		include "petugas/cetak/print_struk_jual.php";
		break;
	case 'hapus_pilihan':
		include "petugas/hapus_pilihan.php";
		break;
	case 'delete':
		include "petugas/hapus_brg_service.php";
		break;
	case 'hapus_jenis_service':
		include "petugas/hapus_jenis_service.php";
		break;
	case 'laporan_service_petugas':
		include "petugas/laporan_service.php";
		break;
	case 'laporan_penjualan_petugas':
		include "petugas/laporan_penjualan.php";
		break;
	case 'laporan_barang_petugas':
		include "petugas/laporan_barang.php";
		break;
	case 'hapus_isi_service':
		include "hapus_isi_service.php";
		break;
	case 'kerjakan':
		include "kerjakan.php";
		break;
	case 'selesaikan':
		include "selesaikan.php";
		break;

		// Admin
	case 'laporan_penjualan':
		include "admin/laporan_penjualan.php";
		break;
	case 'hps_penjualan':
		include "admin/hapus/hapus_penjualan.php";
		break;
	case 'laporan_barang':
		include "admin/laporan_barang.php";
		break;
	case 'laporan_service':
		include "admin/laporan_service.php";
		break;
	case 'hapus_user':
		include "admin/hapus/hapus_user.php";
		break;
	case 'hps_service':
		include "admin/hapus/hapus_service.php";
		break;
		// akhir admin


}
