        <div class="col-md-3 pull-center">
          <!-- Horizontal Form -->
          <div class="box box-info">
           <form action="cetak/cetak_laporan_barang.php" method="post">
          <div class="box-footer">
            <a href="">
          <button value="cetak" name="cetak" type="submit"  class="btn btn-danger pull-right"><i class="fa fa-print"> </i>Cetak PDF</button>
        </a>
      </form>
            <a class="btn btn-default" href="index.php?p=beranda">
                Kembali
              </a>
              </div>
                </div>
          </div>
       

     <div class="col-md-12">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Detail Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th>NO</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Jual</th>
                  <th>Harga Beli</th>
                  <th>Jumlah Stok</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp" . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
                   ?>
                  <?php 
                    include "../koneksi.php";
                   $no=1;
                    $sql=mysqli_query($koneksi,"SELECT  tb_barang.kode_barang,tb_barang.nama_barang,tb_barang.harga_jual,tb_barang.harga_beli,tb_stokbarang.jumlah_stok FROM tb_barang INNER JOIN tb_stokbarang  ON tb_barang.kode_barang=tb_stokbarang.kode_barang");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                  
                <?php if ($view['jumlah_stok'] > 0): ?>
                    
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['kode_barang']; ?></td>
                  <td><?= $view['nama_barang']; ?></td>
                  <td><?= $view['harga_beli']; ?></td>
                  <td><?= $view['harga_jual']; ?></td>
                  <td style="color: red"><?= $view['jumlah_stok']; ?></td>
                  <td>
                    <a href="index.php?p=hps_penjualan&hps_penjualan=<?= $view['id']  ?>&no_faktur=<?= $view['no_faktur'] ?>">
                   <button onclick=" return confirm('Yakin untuk mengahapus?')" type="button" class="btn btn-danger" id="hapus"><i></i>hapus</button>
                 </a>
                 </td>
                  </tr>
                  <?php endif ?>  
                  
              <?php $no++;  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
 
 <script type="text/javascript">
  <?php echo $jsArray; ?>
 function changeValue(id){
 document.getElementById('harga_jual') .value= prdName[id].harga_jual;
            
 };
</script>