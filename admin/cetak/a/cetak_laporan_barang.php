
<?php require('pdf/fpdf.php');?>
<?php
ob_start();
$pdf = new FPDF('l','mm','A5');
$pdf->addPage();
$pdf->SetFont('Times','B',16);
$pdf->Cell(190,5,'LAPORAN BARANG',0,1,'C');
$pdf->Cell(10,3,'',0,1);
$pdf->SetFont('Times','B',12);
$pdf->Cell(190,5,'Bengkel Anes Motor',0,1, 'C');
$pdf->SetFont('Times','B',10);
$pdf->Cell(190,7,'======================================================================================================',0,1, 'C');
function hari_ini(){
	$hari=date("D");
	switch ($hari) {
			case 'Sun':
			$hasil_ini="Minggu";
			break;
			case 'Mon':
			$hasil_ini="Senin";
			break;
			case 'Tue':
			$hasil_ini="Selasa";
			break;
			case 'Wed':
			$hasil_ini="Rabu";
			break;
			case 'Thu':
			$hasil_ini="Kamis";
			break;
			case 'Fri':
			$hasil_ini="Jumat";
			break;
			case 'Sat':
			$hasil_ini="Sabtu";
			break;
		
		default:
			$hasil_ini="Tidak ada";
			break;
	}
	return $hasil_ini;
}
$h=date("D");
$tgl_now=date('d-m-Y  h:i');
$pdf->SetFont('TIMES', '', 10);
$pdf->Ln(0.5);
$pdf->Cell(0, 1,'Tanggal'.' :'.' '.hari_ini($h).' '.' ,' .$tgl_now, 0, 0, 'C');
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','',8);
$pdf->Cell(10,8,'NO',1,0);
$pdf->Cell(45,8,'Nama Barang',1,0);
$pdf->Cell(45,8,'Harga Beli',1,0);
$pdf->Cell(45,8,'Harag Jual',1,0);
$pdf->Cell(45,8,'Jumlah Stok',1,0);

$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','',8);
include "../../koneksi.php";          
function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp. " . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
$sql=mysqli_query($koneksi,"SELECT  tb_barang.kode_barang,tb_barang.nama_barang,tb_barang.harga_jual,tb_barang.harga_beli,tb_stokbarang.jumlah_stok FROM tb_barang INNER JOIN tb_stokbarang  ON tb_barang.kode_barang=tb_stokbarang.kode_barang");
$no=1;
while($tampil=mysqli_fetch_array($sql)) {

	

$pdf->Cell(10,7,$no,1,0,'C');
$pdf->Cell(45,7,$tampil['nama_barang'],1,0);
$pdf->Cell(45,7,rupiah($tampil['harga_jual']),1,0);
$pdf->Cell(45,7,rupiah($tampil['harga_beli']),1,0);
$pdf->Cell(45,7,$tampil['jumlah_stok'],1,0);
$pdf->Cell(10,7,'',0,1);

$no++;
}	
$pdf->Output();
ob_end_flush(); 
?>