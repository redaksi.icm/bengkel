
<?php require('pdf/fpdf.php');?>
<?php
ob_start();
$pdf = new FPDF('l','mm','A5');
$pdf->addPage();
$pdf->SetFont('Times','B',16);
$pdf->Cell(190,5,'LAPORAN PENJUALAN',0,1,'C');
$pdf->Cell(10,3,'',0,1);
$pdf->SetFont('Times','B',12);
$pdf->Cell(190,5,'Bengkel Anes Motor',0,1, 'C');
$pdf->SetFont('Times','B',10);
$pdf->Cell(190,7,'======================================================================================================',0,1, 'C');
function hari_ini(){
	$hari=date("D");
	switch ($hari) {
			case 'Sun':
			$hasil_ini="Minggu";
			break;
			case 'Mon':
			$hasil_ini="Senin";
			break;
			case 'Tue':
			$hasil_ini="Selasa";
			break;
			case 'Wed':
			$hasil_ini="Rabu";
			break;
			case 'Thu':
			$hasil_ini="Kamis";
			break;
			case 'Fri':
			$hasil_ini="Jumat";
			break;
			case 'Sat':
			$hasil_ini="Sabtu";
			break;
		
		default:
			$hasil_ini="Tidak ada";
			break;
	}
	return $hasil_ini;
}
$h=date("D");
$tgl_now=date('d-m-Y  h:i');
$pdf->SetFont('TIMES', '', 10);
$pdf->Ln(0.5);
$pdf->Cell(0, 1,'Tanggal'.' :'.' '.hari_ini($h).', '.' '.$tgl_now, 0, 0, 'C');
$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','',8);
$pdf->Cell(10,8,'NO',1,0);
$pdf->Cell(35,8,'Tanggal',1,0);
$pdf->Cell(25,8,'NO Faktur',1,0);
$pdf->Cell(40,8,'Bama Barang',1,0);
$pdf->Cell(20,8,'Harga',1,0);
$pdf->Cell(20,8,'Jumlah',1,0);
$pdf->Cell(40,8,'Total Harga',1,0);

$pdf->Cell(10,8,'',0,1);
$pdf->SetFont('Times','',8);
include "../../koneksi.php";
if (empty($_POST['tahun'])) {
	echo "<script>
		alert('Upss!! Isi Tahun dan Bulan Boskuu!!');location='../index.php?p=laporan_penjualan';
	</script>";
	
}  elseif (empty($_POST['bulan'])) {
	echo "<script>
		alert('Upss!! Isi Tahun dan Bulan Boskuu!!');location='../index.php?p=laporan_penjualan';
	</script>";
} else{
	$tahun=$_POST['tahun'];
	$bulan=$_POST['bulan'];


}     
     
function rupiah($angka)
                    {
                      $hasil_rupiah= "Rp. " . number_format($angka,2,',','.');
                      return $hasil_rupiah;
                    }
 $sql1=mysqli_query($koneksi,"SELECT * FROM tb_penjualan");
 $cek=mysqli_fetch_array($sql1);
 if (empty($cek)) {
 	echo "<script>
		alert('Penjualan Di Tahun Dan Bulan Tersebut Kosong!');location='../index.php?p=laporan_penjualan';
	</script>";
 }else{

$sql=mysqli_query($koneksi,"SELECT * FROM tb_penjualan where year(tgl)='$tahun' AND month(tgl)= '$bulan'");
 }
$no=1;
while($tampil=mysqli_fetch_array($sql)) {

	$total []=$tampil['total_harga'];
	$h=$tampil['no_faktur'];
	

	

$pdf->Cell(10,7,$no,1,0,'C');
$pdf->Cell(35,7,$tampil['tgl'],1,0);
$pdf->Cell(25,7,$tampil['no_faktur'],1,0);
$pdf->Cell(40,7,$tampil['nama_barang'],1,0);
$pdf->Cell(20,7,rupiah($tampil['harga']),1,0);
$pdf->Cell(20,7,$tampil['jumlah'],1,0);
$pdf->Cell(40,7,rupiah($tampil['total_harga']),1,0);
$pdf->Cell(10,7,'',0,1);

$no++;
}
$sqli=mysqli_query($koneksi,"select sum(total_harga) as total from tb_penjualan where no_faktur='$h'");
$data=mysqli_fetch_array($sqli);
if (empty($data['total']) ){
	echo "<script>
		alert('Penjualan Di Tahun Dan Bulan Tersebut Kosong!');location='../index.php?p=laporan_penjualan';
	</script>";
	
}else{

		$hasil=$data['total'];
}

$pdf->SetFont('TIMES', '', 10);
$pdf->Ln(5);
$pdf->Cell(60, 7,'Total Pendatan'.' '.' : '.rupiah($hasil) , 1, 0, 'C');

$pdf->Output();
ob_end_flush(); 
?>