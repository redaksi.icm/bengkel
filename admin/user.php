
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah User</h3>
            </div>
            <form class="form-horizontal" action="user_tambah.php" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input required=""  type="text" class="form-control" name="nama"  placeholder="Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-10">
                    <input required=""  name="username" type="text" class="form-control"  placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input required=""  name="password" type="text" class="form-control"  placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                   <select  name="level" class="form-control">
                    <option>*pilih salah satu*</option>
                    <option value="admin">Admin</option>
                    <option value="karyawan">Karyawan</option>
                  </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
            <a class="btn btn-default "  href="index.php?p=beranda">
              Cancel
            </a>
            <button value="tambah" name="tambah" type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>

              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
      <div class="row">
        <div class="col-md-6">

                    <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Data User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20px">No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Jabatan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    include "../koneksi.php";
                    $no=1;
                    $sql=mysqli_query($koneksi,"SELECT * FROM user where level='karyawan'");
                    while ($view=mysqli_fetch_array($sql)) {
                    
                   ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $view['nama']; ?></td>
                  <td><?= $view['username']; ?></td>
                  <td><?= $view['password']; ?></td>
                  <td><?= $view['level']; ?></td>
                  <td>
                    
                  <a href="index.php?p=hapus_user&hapus_user=<?= $view['id']; ?>">
                    <button onclick="return confirm('Yakin Untuk Menghapus')" name="edit" type="button" class="btn btn-danger">
                    <i class="fa fa-times-circle"></i></button>
                  </a>
              <a class="btn btn-info" data-toggle="modal" data-target="#modal<?= $view['id'];  ?>" ><i class="fa fa-pencil"></i>
                  
                  </a>
          <div class="modal fade" id="modal<?= $view['id'];  ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data User</h4>
              </div>
              <form class="" action="edit/edit_user.php" method="post">
              <div class="modal-body pull-center">
                 <input  name="id" hidden=""  value="<?= $view['id'];  ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nama User</label>

                  <div class="col-sm-6">
                    <input  value="<?= $view['nama'];  ?>" required=""  type="text" class="form-control" name="nama"  >
                  </div>
                </div>
                <br>
              </div>
            <div class="modal-body">
                <div class="form-group pull-center">
                  <label  for="inputEmail3" class="col-sm-4 control-label">Username</label>

                  <div class="col-sm-8">
                    <input id="rupiah" value="<?= $view['username'];  ?>" required=""  type="text" class="form-control" name="user"  >
                  </div>
                </div>
                <br>
              </div>
              <div class="modal-body">
                <div class="form-group pull-center">
                  <label for="inputEmail3" class="col-sm-4 control-label">Password</label>

                  <div class="col-sm-8">
                    <input id="rupiah" value="<?= $view['password'];  ?>" required=""  type="text" class="form-control" name="pass"  >
                  </div>
                </div>
                <br>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" type="button" class="btn btn-primary">Save changes</button>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
                  </td>
                </tr>
              <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <!-- /.row -->